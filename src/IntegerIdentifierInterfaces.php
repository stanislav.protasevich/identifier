<?php

declare(strict_types = 1);

namespace ProDevZone\Common\Identifier;

/**
 * Interface IntegerIdentifierInterfaces
 * @package ProDevZone
 */
interface IntegerIdentifierInterfaces extends IdentifierInterface
{
    /**
     * @param int $identifier
     * @return IdentifierInterface
     */
    public static function fromInteger(int $identifier): IdentifierInterface;

    /**
     * @param string $identifier
     * @return IdentifierInterface
     */
    public static function fromString(string $identifier): IdentifierInterface;
}
