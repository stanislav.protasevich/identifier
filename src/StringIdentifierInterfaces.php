<?php

declare(strict_types = 1);

namespace ProDevZone\Common\Identifier;

/**
 * Interface StringIdentifierInterfaces
 * @package ProDevZone
 */
interface StringIdentifierInterfaces extends IdentifierInterface
{
    /**
     * @param string $identifier
     * @return IdentifierInterface
     */
    public static function fromString(string $identifier): IdentifierInterface;
}
