<?php

declare(strict_types = 1);

namespace ProDevZone\Common\Identifier;

/**
 * Class IntegerIdentifier
 * @package ProDevZone\Common\Identifier
 */
class IntegerIdentifier implements IntegerIdentifierInterfaces
{
    /** @var string */
    protected $identifier;

    /**
     * IntegerIdentifier constructor.
     * @param $identifier
     */
    protected function __construct($identifier)
    {
        $this->setIdentifier($identifier);
    }

    /**
     * @param int $identifier
     */
    protected function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return (string) $this->identifier;
    }

    /**
     * @inheritdoc
     */
    public static function fromInteger(int $identifier): IdentifierInterface
    {
        return new static($identifier);
    }

    /**
     * @inheritdoc
     */
    public static function fromString(string $identifier): IdentifierInterface
    {
        return new static($identifier);
    }
}
