<?php

declare(strict_types = 1);

namespace ProDevZone\Common\Identifier;

/**
 * Class IdentifiableDomainObject
 * @package ProDevZone\Ddd\Domain\Model
 */
abstract class IdentifiableEntity
{
    /** @var IdentifierInterface */
    protected $id;

    /**
     * @return IdentifierInterface
     */
    protected function getId(): IdentifierInterface
    {
        return $this->id;
    }

    /**
     * @param IdentifierInterface $identifier
     * @return IdentifiableEntity
     */
    protected function setId(IdentifierInterface $identifier): self
    {
        $this->id = $identifier;

        return $this;
    }
}
