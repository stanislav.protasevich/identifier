<?php

declare(strict_types = 1);

namespace ProDevZone\Common\Identifier;

/**
 * Interface IdentifierInterface
 * @package ProDevZone\Common\Identifier
 */
interface IdentifierInterface
{
    /**
     * @return string
     */
    public function __toString(): string;
}
